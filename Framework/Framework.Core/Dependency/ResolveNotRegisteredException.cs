﻿using System;

namespace Framework.Core
{
	public class ResolveNotRegisteredException : Exception
	{
		public ResolveNotRegisteredException(Exception e, Type type) : base(string.Format("{0} implementation not registered", type.Name), e)
		{
		}
	}
}

