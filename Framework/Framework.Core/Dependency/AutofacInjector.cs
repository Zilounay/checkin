﻿using System;
using Autofac;
using Autofac.Core;
using System.Collections.Generic;
using Autofac.Builder;
using System.Linq;
using System.Reflection;
using Autofac.Core.Registration;

namespace Framework.Core
{
	public class AutofacInjector : IDependencyInjector
	{
		readonly IContainer container;

		public AutofacInjector()
		{
			var builder = new ContainerBuilder();

			container = builder.Build();
		}

		#region IDependencyInjector

	    public IDependencyInjector RegisterType<TType>()
			where TType : class
		{
			var containerBuilder = new ContainerBuilder();
			containerBuilder.RegisterType<TType>();
			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterFactory<TInterface>(Func<TInterface> del)
			where TInterface : class
		{
			var containerBuilder = new ContainerBuilder();
			containerBuilder.Register(d => del());
			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterFactory<TImplementation, TInterface>()
			where TInterface : class
			where TImplementation : class, TInterface
		{
			var containerBuilder = new ContainerBuilder();
			containerBuilder.RegisterType<TImplementation>().As<TInterface>();
			containerBuilder.Update(container);
			return this;
		}

	    public IDependencyInjector RegisterFactory<TInterface, TImplementation0, TImplementation1>(int whichImplementation)
	        where TInterface : class
	        where TImplementation0 : class, TInterface
	        where TImplementation1 : class, TInterface
	    {
	        if (whichImplementation == 0)
	            return RegisterFactory<TImplementation0, TInterface>();
	        else
	            return RegisterFactory<TImplementation1, TInterface>();
	    }

	    public IDependencyInjector RegisterSingleton<TImplementation>()
			where TImplementation : class
		{
			var containerBuilder = new ContainerBuilder();
			/*var registrationOk =*/ containerBuilder.RegisterType<TImplementation>().SingleInstance();
			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterSingleton<TImplementation, TInterface>(bool injectAllInterfaces = false)
			where TInterface : class
			where TImplementation : class, TInterface
		{
			var containerBuilder = new ContainerBuilder();
			var registration = containerBuilder.RegisterType<TImplementation>().As<TInterface>().SingleInstance();

			if (injectAllInterfaces)
			{
				registration.AsImplementedInterfaces();
			}

			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterSingleton<TInterface>(Type type, bool injectAllInterfaces = false)
			where TInterface : class
		{
			var containerBuilder = new ContainerBuilder();
			var registration = containerBuilder.RegisterType(type).As<TInterface>().SingleInstance(); ;

			if (injectAllInterfaces)
			{
				registration.AsImplementedInterfaces();
			}

			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterSingletonFromInstance<T>(T instance, bool injectAllInterfaces = false)
			where T : class
		{
			var containerBuilder = new ContainerBuilder();

			if (injectAllInterfaces)
			{
				containerBuilder.RegisterInstance(instance).AsImplementedInterfaces();
			}
			else
			{
				containerBuilder.RegisterInstance(instance).As<T>();
			}

			containerBuilder.Update(container);
			return this;
		}

		public IDependencyInjector RegisterAll(Assembly assembly, string searchFor = null, bool asSingletons = true)
		{
			var containerBuilder = new ContainerBuilder();
			var registrationBuilder = containerBuilder.RegisterAssemblyTypes(assembly);

			if (searchFor != null)
			{
				registrationBuilder = registrationBuilder.Where(t => t.Name.EndsWith(searchFor, StringComparison.Ordinal));
			}

			registrationBuilder.AsImplementedInterfaces();

			if (asSingletons)
			{
				registrationBuilder.SingleInstance();
			}

			containerBuilder.Update(container);

			return this;
		}

		public T Resolve<T>()
			where T : class
		{
			try
			{
				return container.Resolve<T>();
			}
			catch (ComponentNotRegisteredException ex)
			{
				throw new ResolveNotRegisteredException(ex, typeof(T));
			}
		}

		public T Resolve<T, TP>(object parameter)
			where T : class
		{
			try
			{
				return container.Resolve<T>(new TypedParameter(typeof(TP), parameter));
			}
			catch (ComponentNotRegisteredException ex)
			{
				throw new ResolveNotRegisteredException(ex, typeof(T));
			}
		}

		public bool CanResolve<T>()
			where T : class
		{
			return container.IsRegistered<T>();
		}

		public T Resolve<T>(Type t) where T : class
		{
			return (T)container.Resolve(t);
		}

		#endregion


		class ResolveAnythingSource : IRegistrationSource
		{
			public IEnumerable<IComponentRegistration> RegistrationsFor(
				Service service,
				Func<Service, IEnumerable<IComponentRegistration>> registrationAccessor)
			{
				TypedService typedService = service as TypedService;
				if (typedService != null)
				{
					var registrationBuilder = RegistrationBuilder.ForType(typedService.ServiceType);
					return new[] { RegistrationBuilder.CreateRegistration(registrationBuilder) };
				}

				return Enumerable.Empty<IComponentRegistration>();
			}

			public bool IsAdapterForIndividualComponents
			{
				get
				{
					return false;
				}
			}
		}
	}
}