﻿using System;

namespace Framework.Core
{
	public static class DependencyExtensions
	{
		public static IDependencyInjector DIRegisterType<TType>(this object thiz)
			where TType : class
		{
			return Dependency.Instance.RegisterType<TType>();
		}
		public static IDependencyInjector DIRegisterFactory<TC, T>(this object thiz)
			where T : class
			where TC : class, T
		{
			return Dependency.Instance.RegisterFactory<TC, T>();
		}
		public static IDependencyInjector DIRegisterSingleton<TC, T>(this object thiz)
			where T : class
			where TC : class, T
		{
			return Dependency.Instance.RegisterSingleton<TC, T>();
		}
		public static IDependencyInjector RegisterSingletonFromInstance<T>(this object thiz, T obj, bool injectAllInterfaces = false)
			where T : class
		{
			return Dependency.Instance.RegisterSingletonFromInstance(obj, injectAllInterfaces);
		}

		public static T DIResolve<T>(this object thiz)
			where T : class
		{
			return Dependency.Instance.Resolve<T>();
		}
		public static T DIResolve<T, TP>(this object thiz, Object parameter)
			where T : class
		{
			return Dependency.Instance.Resolve<T, TP>(parameter);
		}
	}
}