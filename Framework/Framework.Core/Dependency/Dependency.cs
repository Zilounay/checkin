﻿using System;

namespace Framework.Core
{
	public static class Dependency
	{
		static IDependencySetup[] _setups;

		public static IDependencyInjector Initialize(IDependencyInjector injectorInstance)
		{
			Instance = injectorInstance;

			return injectorInstance;
		}

		public static IDependencyInjector Initialize(IDependencyInjector injectorInstance, params IDependencySetup[] setups)
		{
			if (_instance == null)
			{
				injectorInstance = Initialize(injectorInstance);
				_setups = setups;

				SetupSetups();
				InitializeSetups();
			}
			return injectorInstance;
		}

		static void SetupSetups()
		{
			foreach (IDependencySetup setup in _setups)
			{
				setup.Setup(Instance);
			}
		}

		static void InitializeSetups()
		{
			foreach (IDependencySetup setup in _setups)
			{
				setup.Initialize(Instance);
			}
		}

		static IDependencyInjector _instance;

		public static IDependencyInjector Instance
		{
			get
			{
				if (_instance == null)
				{
					throw new NullReferenceException("Dependency.Instance is null, did you forget to initialize it ? example -> Dependency.Initialize(new AutofacInjector(), new SetupTest());");
				}

				return _instance;
			}
			private set
			{
				_instance = value;
			}
		}
	}
}