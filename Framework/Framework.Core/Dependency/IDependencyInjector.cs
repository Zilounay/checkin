using System;
using System.Reflection;

namespace Framework.Core
{
	public interface IDependencyInjector
	{
		IDependencyInjector RegisterSingleton<TImplementation>()
			where TImplementation : class
			;

		IDependencyInjector RegisterSingleton<TImplementation, TInterface>(bool injectAllInterfaces = false)
			where TInterface : class
			where TImplementation : class, TInterface
			;

		IDependencyInjector RegisterSingleton<TInterface>(Type type, bool injectAllInterfaces = false)
			where TInterface : class
			;
		
		IDependencyInjector RegisterSingletonFromInstance<T>(T instance, bool injectAllInterfaces = false) where T : class;

		IDependencyInjector RegisterFactory<TInterface>(Func<TInterface> del)
			where TInterface : class
		;

	    IDependencyInjector RegisterFactory<TImplementation, TInterface>()
	        where TInterface : class
	        where TImplementation : class, TInterface
	    ;

	    IDependencyInjector RegisterFactory<TInterface, TImplementation0, TImplementation1>(int whichImplementation)
	        where TInterface : class
	        where TImplementation0 : class, TInterface
	        where TImplementation1 : class, TInterface
	    ;

	    IDependencyInjector RegisterType<TType>()
			where TType : class
			;

		IDependencyInjector RegisterAll(Assembly assembly, string searchFor = null, bool asSingletons = true);

		T Resolve<T>()
			where T : class
			;

		T Resolve<T, TP>(object parameter)
			where T : class
			;

		bool CanResolve<T>()
			where T : class
			;

		T Resolve<T>(Type t)
			where T : class
			;
	}
}