﻿namespace Framework.Core
{
	public interface IDependencySetup
	{
		void Setup(IDependencyInjector injector);

		void Initialize(IDependencyInjector injector);
	}
}