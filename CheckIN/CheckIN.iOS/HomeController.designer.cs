// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace CheckIN.iOS
{
	[Register ("HomeController")]
	partial class HomeController
	{
		[Outlet]
		UIKit.UIButton CheckinButton { get; set; }

		[Action ("CheckinTapped:")]
		partial void CheckinTapped (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (CheckinButton != null) {
				CheckinButton.Dispose ();
				CheckinButton = null;
			}
		}
	}
}
