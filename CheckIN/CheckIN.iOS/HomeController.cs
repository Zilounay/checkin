using System;
using CheckIN.Core;
using Foundation;
using Framework.Core;
using UIKit;

namespace CheckIN.iOS
{
	public partial class HomeController : UIViewController
	{
		public HomeController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			IHomeViewModel viewModel = Dependency.Instance.Resolve<IHomeViewModel>();

			CheckinButton.SetTitle(viewModel.Text, UIControlState.Normal);
		}

		partial void CheckinTapped(NSObject sender)
		{

		}
	}
}