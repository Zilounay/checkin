﻿using Framework.Core;

namespace CheckIN.Core
{
	public abstract class SetupCore : IDependencySetup
	{
		public virtual void Setup(IDependencyInjector injector)
		{
			injector.RegisterFactory<HomeViewModel, IHomeViewModel>();
			injector.RegisterFactory<SubscriptionViewModel, ISubscriptionViewModel>();
		}

		public virtual void Initialize(IDependencyInjector injector)
		{
			injector = injector ?? Dependency.Instance;
		}
	}
}